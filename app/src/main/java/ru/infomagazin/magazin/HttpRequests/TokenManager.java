package ru.infomagazin.magazin.HttpRequests;

public class TokenManager {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
