package ru.infomagazin.magazin.HttpRequests;

import android.annotation.TargetApi;
import android.os.Build;
import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class Authentication {

    /*
            Авторизация  ..  Метод выдает пользователю TOKEN
    */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public String http_auth(String username, String password) throws IOException, JSONException {
        String url = "https://tanya.tracex.ru/api/login";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        String auth = username + ":" + password;

        byte[] data1 = auth.getBytes(StandardCharsets.UTF_8);
        String base64 = Base64.encodeToString(data1, Base64.NO_WRAP);

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-type", "application/json");
        con.setRequestProperty("Authorization", "Basic " + base64);

        //Создание Json данных:  {""username":"username","password":"password"}
        JSONObject urlParameters = new JSONObject();

        urlParameters.put("username", username);
        urlParameters.put("password", password);

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(String.valueOf(urlParameters));
        wr.flush();
        wr.close();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }

}
