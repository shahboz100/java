package ru.infomagazin.magazin.HttpRequests;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.json.JSONObject;

public class HttpRequest {

    /** Получение заказов */
    public JSONObject getOrders(String token, String role) throws Exception {
        String url = "https://tanya.tracex.ru/api/order";
        if (role=="ROLE_PROVIDER"){
            url+="/my";
        }
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is GET
        con.setRequestMethod("GET");
        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestProperty("access-token", token);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return new JSONObject(response.toString());
    }

    /** Полечение коментариев */
    public JSONObject getComments(String token, String uri) throws Exception {
        String url = "https://tanya.tracex.ru/api/comment/" + uri;
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is GET
        con.setRequestMethod("GET");
        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestProperty("access-token", token);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return new JSONObject(response.toString());
    }

    /** Отправка на сервер нового коментария */
    public void setComment(String token, String uri, String comment) throws Exception {

        String url = "https://tanya.tracex.ru/api/comment/add/" + uri;
        URL obj = new URL(url);

        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is POST
        con.setRequestMethod("POST");
        // add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestProperty("Accept-Charset", "UTF-8");
        con.setRequestProperty("Content-type", "application/json;charset=UTF-8");
        con.setRequestProperty("access-token", token);


        JSONObject urlParameters = new JSONObject();

        urlParameters.put("comment", "'\'"+comment+"'\'");

        // Send post request
        con.setDoOutput(true);
        OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
        wr.write(String.valueOf(urlParameters));
        wr.flush();
        wr.close();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
    }

    /** Удаление коментария */
    public JSONObject deleteComment(String token, String id) throws Exception {

            String url = "https://tanya.tracex.ru/api/comment/delete/" + id;
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            // optional default is GET
            con.setRequestMethod("POST");
            //add request header
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            con.setRequestProperty("access-token", token);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return new JSONObject(response.toString());
    }

    /** Подтверждение заказа */
    public JSONObject confirmOrder(String token, String id) throws Exception {

        String url = "https://tanya.tracex.ru/api/order/confirm/" + id;
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is GET
        con.setRequestMethod("GET");
        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestProperty("Content-type", "application/json");
        con.setRequestProperty("access-token", token);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return new JSONObject(response.toString());
    }

    /** Получение информации профиля */
    public JSONObject getProfile(String token) throws Exception {

        String url = "https://tanya.tracex.ru/api/user/profile";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is GET
        con.setRequestMethod("GET");
        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestProperty("Content-type", "application/json");
        con.setRequestProperty("access-token", token);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return new JSONObject(response.toString());
    }

    /** Регистрация нового пользователя */
    public JSONObject register(String[] arr) throws Exception {

        String url = "https://tanya.tracex.ru/api/user/register";
        URL obj = new URL(url);

        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is POST
        con.setRequestMethod("POST");
        // add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestProperty("Content-type", "application/json;charset=UTF-8");


        JSONObject urlParameters = new JSONObject();

        urlParameters.put("login", arr[0]);
        urlParameters.put("fio", arr[1]);
        urlParameters.put("email", arr[2]);
        urlParameters.put("organization", arr[3]);
        urlParameters.put("address", arr[4]);
        urlParameters.put("password", arr[5]);
        urlParameters.put("phone", arr[6]);
        urlParameters.put("provider", Integer.parseInt(arr[7]));
        System.out.println(urlParameters);
        // Send post request
        con.setDoOutput(true);
        OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
        wr.write(String.valueOf(urlParameters));
        wr.flush();
        wr.close();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return new JSONObject(response.toString());
    }

    /** Получение списка товаров */
    public JSONObject getProduct(String token) throws Exception {

        String url = "https://tanya.tracex.ru/api/product";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is GET
        con.setRequestMethod("GET");
        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestProperty("Content-type", "application/json");
        con.setRequestProperty("access-token", token);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return new JSONObject(response.toString());
    }

    /** Запись идентификатора телефона в Firebase */
    public void setKeyFirebase(String token, String key) throws Exception {

        String url = "https://tanya.tracex.ru/api/user/notification";
        URL obj = new URL(url);

        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is POST
        con.setRequestMethod("POST");
        // add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestProperty("Accept-Charset", "UTF-8");
        con.setRequestProperty("Content-type", "application/json;charset=UTF-8");
        con.setRequestProperty("access-token", token);


        JSONObject urlParameters = new JSONObject();

        urlParameters.put("key", key);

        // Send post request
        con.setDoOutput(true);
        OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
        wr.write(String.valueOf(urlParameters));
        wr.flush();
        wr.close();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
    }

}