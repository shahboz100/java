package ru.infomagazin.magazin;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import ru.infomagazin.magazin.HttpRequests.HttpRequest;

public class RegisterActivity extends AppCompatActivity {

    private TextView login;
    private TextView FIO;
    private TextView email;
    private TextView org;
    private TextView address;
    private TextView password;
    private TextView password1;
    private TextView phone;
    private TextView error;
    private boolean register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    @Override
    protected void onStart() {
        super.onStart();
        login = findViewById(R.id.registrationLOGIN);
        FIO = findViewById(R.id.registrationFIO);
        email = findViewById(R.id.registrationEMAIL);
        org = findViewById(R.id.registrationORGANIZATION);
        address = findViewById(R.id.registrationADDRESS);
        password = findViewById(R.id.registrationPASSWD);
        password1 = findViewById(R.id.registrationPASSWD1);
        phone = findViewById(R.id.registrationPHONE);
        error = findViewById(R.id.registrationHINTERR);
        error.setVisibility(View.GONE);
    }

    public void onRadioButtonProvider(View view) {
        register = true;
    }

    public void onRadioButtonClient(View view) {
        register = false;
    }

    public void onClickRegister(View view) {
        if (!password.getText().toString().equals(password1.getText().toString())){
            error.setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(),
                    "Пароли не совпадают!", Toast.LENGTH_SHORT).show();
        }else{
            Reg reg = new Reg();
            reg.execute();
        }
    }

    class Reg extends AsyncTask<Void,Void, JSONObject>{

        @Override
        protected JSONObject doInBackground(Void... voids) {
            HttpRequest httpRequest = new HttpRequest();
            String [] arr = new String[9];
            arr[0] = login.getText().toString();
            arr[1] = FIO.getText().toString();
            arr[2] = email.getText().toString();
            arr[3] = org.getText().toString();
            arr[4] = address.getText().toString();
            arr[5] = password.getText().toString();
            arr[6] = phone.getText().toString();
            if (register){
                arr[7]="1";
            }else {
                arr[7]="0";
            }
            try {
               return httpRequest.register(arr);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            if (result.has("message")){
                try {
                    Toast.makeText(getApplicationContext(),
                            result.getString("message").toString(), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (result.has("status")){
                Intent intent = new Intent(RegisterActivity.this, ConfirmActivity.class);
                startActivity(intent);
            }
        }
    }
}
