package ru.infomagazin.magazin.Model;

public class CommentModel {

    public String id;
    public String author;
    public String date;
    public String comment;
    public String time;

    public CommentModel(String id, String author, String date, String comment, String time){
        this.id = id;
        this.author = author;
        this.date = date;
        this.comment = comment;
        this.time = time;
    }

}
