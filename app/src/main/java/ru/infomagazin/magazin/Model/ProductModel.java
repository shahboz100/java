package ru.infomagazin.magazin.Model;

public class ProductModel {

    public String id;
    public String name;
    public String price;
    public String count;
    public String category;

    public ProductModel(String id, String name, String price, String count, String category) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.count = count;
        this.category = category;

    }
}
