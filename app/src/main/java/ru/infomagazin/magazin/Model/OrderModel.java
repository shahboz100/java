package ru.infomagazin.magazin.Model;

public class OrderModel {
    public int id;
    public String product;
    public String client;
    public String provider;
    public String date;
    public String createDate;
    public int count;
    public String comment;
    public boolean confirm;

    public OrderModel(int id, String product, String client, String provider, String date, String createDate, int count, String comment, boolean confirm) {
        this.id = id;
        this.product = product;
        this.client = client;
        this.provider = provider;
        this.date = date;
        this.createDate = createDate;
        this.count = count;
        this.comment = comment;
        this.confirm = confirm;
    }
}
