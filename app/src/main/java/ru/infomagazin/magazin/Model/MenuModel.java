package ru.infomagazin.magazin.Model;

public class MenuModel {
    public String id;
    public String name;
    public String url;

    public MenuModel(String id, String name, String url){
        this.id = id;
        this.name = name;
        this.url = url;
    }
}
