package ru.infomagazin.magazin.Fragments;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ru.infomagazin.magazin.Adapters.CommentAdapter;
import ru.infomagazin.magazin.HttpRequests.HttpRequest;
import ru.infomagazin.magazin.MainActivity;
import ru.infomagazin.magazin.Model.CommentModel;
import ru.infomagazin.magazin.R;

public class CommentFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    String token;
    String id;
    CommentAdapter commentAdapter;
    public SwipeRefreshLayout swipeLayout;
    RecyclerView commentRecyclerView;
    EditText commentEdit;

    public static CommentFragment newInstance() {
        return new CommentFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        commentAdapter = new CommentAdapter(clickListener);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        assert getArguments() != null;
        id = getArguments().getString("id", "0");
        token = getArguments().getString("token");
        View view = inflater.inflate(R.layout.fragment_comment, container, false);
        commentEdit = view.findViewById(R.id.editComment);
        ImageButton button1 = view.findViewById(R.id.imageBtnSend);
        button1.setOnClickListener(this);

        swipeLayout = view.findViewById(R.id.swipe_container_comment);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeColors(android.R.color.holo_green_dark,
                android.R.color.holo_red_dark,
                android.R.color.holo_blue_dark,
                android.R.color.holo_orange_dark);

        return view;
    }

    final View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DeleteDialog deleteDialog = new DeleteDialog();
            deleteDialog.product = id;
            ((MainActivity) Objects.requireNonNull(getActivity())).onSelectedDialogDelete(v.getTag().toString());
        }
    };



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        CommentTask commentTask = new CommentTask(view);
        commentTask.execute(id);
        swipeLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {

        CommentTask commentTask = new CommentTask(swipeLayout);
        commentTask.execute(id);

        swipeLayout.setRefreshing(false);
    }

    @Override
    public void onClick(View v) {

        AddComment addComment = new AddComment();
        String[] str = new String[2];
        str[0] = id;
        str[1] = commentEdit.getText().toString();
        addComment.execute(str);
        commentEdit.setText("");

        CommentTask commentTask = new CommentTask(swipeLayout);
        commentTask.execute(id);
    }

    @SuppressLint("StaticFieldLeak")
    class CommentTask extends AsyncTask<String, Void, JSONObject> {

        private View view;

        CommentTask(View view){
            this.view = view;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //TODO: Кругляшок
        }

        @Override
        protected JSONObject doInBackground(String... url) {

            HttpRequest httpRequest = new HttpRequest();

            try {
                return httpRequest.getComments(token, url[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            commentRecyclerView = view.findViewById(R.id.commentListView);
            commentRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            commentRecyclerView.setAdapter(commentAdapter);

            List<CommentModel> commentModels = new ArrayList<>();

            try {
                if (result.has("comments")) {
                    JSONArray jsonArray = result.getJSONArray("comments");
                    for (int i=0; i<jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        commentModels.add(
                                new CommentModel(
                                        jsonObject.getString("id"),
                                        jsonObject.getString("author"),
                                        jsonObject.getString("date"),
                                        jsonObject.getString("comment"),
                                        jsonObject.getString("time")));
                    }
                    if (jsonArray.length()>9){
                        commentModels.add(new CommentModel("","","","",""));
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            commentAdapter.setCommentList(commentModels);
        }
    }

    class AddComment extends AsyncTask<String,Void, Void>{

        @Override
        protected Void doInBackground(String[] url) {

            HttpRequest httpRequest = new HttpRequest();
            try {
                httpRequest.setComment(token, url[0], url[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }
}

