package ru.infomagazin.magazin.Fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;

import ru.infomagazin.magazin.HttpRequests.HttpRequest;
import ru.infomagazin.magazin.R;

public class ProfileFragment extends Fragment {

    String id;
    String token;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        return inflater.inflate(R.layout.profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assert getArguments() != null;
        id = getArguments().getString("id");
        token = getArguments().getString("token");
        ProfileTask profileTask = new ProfileTask(getView());
        profileTask.execute();
    }

    class ProfileTask extends AsyncTask<String,Void, JSONObject>{

        private View view;

        ProfileTask(View view){
            this.view = view;
        }

        @Override
        protected JSONObject doInBackground(String... strings) {

            HttpRequest httpRequest = new HttpRequest();

            try {
                return httpRequest.getProfile(token);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            try {
                JSONObject jsonObject = result.getJSONObject("user");
                TextView fio = view.findViewById(R.id.FIO);
                TextView login = view.findViewById(R.id.blood_group);
                TextView ident = view.findViewById(R.id.education);
                TextView email = view.findViewById(R.id.occupation);
                TextView phone = view.findViewById(R.id.mobileNumber);
                TextView role = view.findViewById(R.id.gender);
                fio.setText(jsonObject.getString("fio"));
                login.setText(jsonObject.getString("login"));
                ident.setText(jsonObject.getString("id"));
                email.setText(jsonObject.getString("email"));
                phone.setText(jsonObject.getString("phone"));
                role.setText(jsonObject.getString("role"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
