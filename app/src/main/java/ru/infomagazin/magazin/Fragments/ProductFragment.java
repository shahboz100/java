package ru.infomagazin.magazin.Fragments;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ru.infomagazin.magazin.Adapters.ProductAdapter;
import ru.infomagazin.magazin.HttpRequests.HttpRequest;
import ru.infomagazin.magazin.Model.ProductModel;
import ru.infomagazin.magazin.R;

public class ProductFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    ProductAdapter productAdapter;
    public String ROLE;
    SwipeRefreshLayout swipeLayout;
    public String token;
    RecyclerView productRecyclerView;

    public static ProductFragment newInstance() {
        return new ProductFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        productAdapter = new ProductAdapter(clickListener);
    }

    View.OnClickListener clickListener = v -> {

    };

    @SuppressLint("ResourceAsColor")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        assert getArguments() != null;
        token = getArguments().getString("token");
        View view = inflater.inflate(R.layout.product_fragment, container, false);

        swipeLayout = view.findViewById(R.id.swipe_container_product);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeColors(android.R.color.holo_green_dark,
                android.R.color.holo_red_dark,
                android.R.color.holo_blue_dark,
                android.R.color.holo_orange_dark);

        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ProductTask productTask = new ProductTask(getView());
        productTask.execute();
    }

    @Override
    public void onRefresh() {
        ProductTask productTask = new ProductTask(getView());
        productTask.execute();
        swipeLayout.setRefreshing(false);
    }

    class ProductTask extends AsyncTask<String, Void, JSONObject> {

        private View view;

        private ProductTask(View view) {
            this.view = view;
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            HttpRequest httpRequest = new HttpRequest();

            try {
                return httpRequest.getProduct(token);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            productRecyclerView = view.findViewById(R.id.listview);
            productRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            productRecyclerView.setAdapter(productAdapter);
            List<ProductModel> productModels = new ArrayList<>();

            try {
                if (result.has("products")) {
                    JSONArray jsonArray = result.getJSONArray("products");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        productModels.add(
                                new ProductModel(
                                        jsonObject.getString("id"),
                                        jsonObject.getString("name"),
                                        jsonObject.getString("price"),
                                        jsonObject.getString("count"),
                                        jsonObject.getString("category")
                                )
                        );
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            productAdapter.setProductList(productModels);
        }
    }
}
