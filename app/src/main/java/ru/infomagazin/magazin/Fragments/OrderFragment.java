package ru.infomagazin.magazin.Fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ru.infomagazin.magazin.Adapters.OrderAdapter;
import ru.infomagazin.magazin.HttpRequests.HttpRequest;
import ru.infomagazin.magazin.MainActivity;
import ru.infomagazin.magazin.Model.OrderModel;
import ru.infomagazin.magazin.R;


public class OrderFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    SwipeRefreshLayout swipeLayout;
    String token;
    String ROLE;
    RecyclerView orderRecyclerView;
    OrderAdapter orderAdapter;


    public OrderFragment() {
    }


    public static OrderFragment newInstance() {
        return new OrderFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orderAdapter = new OrderAdapter(clickListener);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        assert getArguments() != null;
        token = getArguments().getString("token");
        ROLE = getArguments().getString("role");
        View view = inflater.inflate(R.layout.fragment_order, container, false);

        swipeLayout = view.findViewById(R.id.swipe_container_order);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeColors(android.R.color.holo_green_dark,
                android.R.color.holo_red_dark,
                android.R.color.holo_blue_dark,
                android.R.color.holo_orange_dark);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        OrderTask mt = new OrderTask(view);
        mt.execute(token);
    }

    View.OnClickListener clickListener = v -> {

        switch (v.getId()){
            case R.id.confirmIcon:
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Вы действительно хотите подтвердить?");
                builder.setPositiveButton("Отмена",
                                (dialog, id) -> dialog.cancel())
                        .setNegativeButton("Подтверждаю",
                                (dialog, which) -> {
                                    OrderConfirm orderConfirm = new OrderConfirm();
                                    orderConfirm.execute(v.getTag().toString());
                                    onRefresh();
                                });
                AlertDialog alert = builder.create();
                alert.show();
                break;
            case R.id.commentIcon:
                ((MainActivity) Objects.requireNonNull(getActivity())).onNavigationItemSelected(v.getTag().toString());
                break;
            default:
                OrderFragmentFull orderFragmentFull = new OrderFragmentFull();
                FragmentTransaction ftrans = null;
                if (getFragmentManager() != null) {
                    ftrans = getFragmentManager().beginTransaction();
                }

                Bundle bundle = new Bundle();

                TextView product = v.findViewById(R.id.product);
                TextView col = v.findViewById(R.id.count);
                TextView dateP = v.findViewById(R.id.datePString);
                TextView dateZ = v.findViewById(R.id.dateSozdaniya);
                TextView client = v.findViewById(R.id.client);

                bundle.putString("name", product.getText().toString());
                bundle.putString("count", col.getText().toString());
                bundle.putString("dateP", dateP.getText().toString());
                bundle.putString("dateZ", dateZ.getText().toString());
                bundle.putString("client", client.getText().toString());
                bundle.putString("note", client.getText().toString());
                bundle.putString("tag", v.getTag().toString());
                orderFragmentFull.setArguments(bundle);
                Objects.requireNonNull(ftrans).replace(R.id.container, orderFragmentFull).addToBackStack("back").commit();

        }
    };

    @Override
    public void onRefresh() {
        OrderTask mt = new OrderTask(swipeLayout);
        mt.execute(token);
        swipeLayout.setRefreshing(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainActivity) Objects.requireNonNull(getActivity())).onVisibleNavigation(true);
    }

    class OrderConfirm extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... strings) {
            HttpRequest httpRequest = new HttpRequest();
            try {
                return httpRequest.confirmOrder(token, strings[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            if (result.has("message")){
                Toast toast = null;
                try {
                    toast = Toast.makeText(getActivity(),
                            result.getString("message").toString(), Toast.LENGTH_SHORT);
                    toast.show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    @SuppressLint("StaticFieldLeak")
    class OrderTask extends AsyncTask<String, Void, JSONObject> {

        private View view;

        OrderTask(View view){
            this.view = view;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Кругляшок
        }

        @Override
        protected JSONObject doInBackground(String... url) {

            HttpRequest httpRequest = new HttpRequest();

            try {
               return httpRequest.getOrders(token, ROLE);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            orderRecyclerView = view.findViewById(R.id.listview);
            orderRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            orderRecyclerView.setAdapter(orderAdapter);
            List<OrderModel> orderModels = new ArrayList<>();

            try {
                if (result.has("orders")) {
                    JSONArray jsonArray = result.getJSONArray("orders");
                    for (int i=0; i<jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                            orderModels.add(
                                    new OrderModel(
                                            Integer.parseInt(jsonObject.getString("id")),
                                            jsonObject.getString("product"),
                                            jsonObject.getString("client"),
                                            "",
                                            jsonObject.getString("date"),
                                            jsonObject.getString("createDate"),
                                            Integer.parseInt(jsonObject.getString("count")),
                                            jsonObject.getString("comment"),
                                            Boolean.parseBoolean(jsonObject.getString("confirm"))
                                    ));
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            orderAdapter.setDriverList(orderModels);
        }
    }
}