package ru.infomagazin.magazin.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ru.infomagazin.magazin.Adapters.MenuAdapter;
import ru.infomagazin.magazin.MainActivity;
import ru.infomagazin.magazin.Model.MenuModel;
import ru.infomagazin.magazin.R;

public class MenuFragment extends Fragment {

    RecyclerView menuRecyclerView;
    MenuAdapter menuAdapter;

    String token;

    public MenuFragment() {
    }


    public static MenuFragment newInstance() {
        return new MenuFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        menuAdapter = new MenuAdapter(clickListener);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_menu, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assert getArguments() != null;

        token = getArguments().getString("token");

        menuRecyclerView = view.findViewById(R.id.fragment_menu_listview);
        menuRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        menuRecyclerView.setAdapter(menuAdapter);
        List<MenuModel> menuModels = new ArrayList<>();
        menuModels.add(new MenuModel("1","Главная","http://dddd.dd"));
        menuModels.add(new MenuModel("2","Товары","http://dddd.dd"));
        menuModels.add(new MenuModel("3","Заказы","http://dddd.dd"));
        menuModels.add(new MenuModel("4","Настройки","http://dddd.dd"));
        menuModels.add(new MenuModel("5","Выход","http://dddd.dd"));

        menuAdapter.setMenuList(menuModels);
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Bundle bundle = new Bundle();
            FragmentTransaction ftrans = null;
            switch (v.getTag().toString()){
                case ("1"):
                    bundle.clear();
                    bundle.putString("token", token);
                    ProfileFragment profileFragment = new ProfileFragment();

                    if (getFragmentManager() != null) {
                        ftrans = getFragmentManager().beginTransaction();
                    }
                    profileFragment.setArguments(bundle);
                    Objects.requireNonNull(ftrans).replace(R.id.container, profileFragment).addToBackStack("back").commit();
                    break;
                case ("2"):
                    bundle.clear();
                    bundle.putString("token", token);
                    ProductFragment productFragment = new ProductFragment();
                    if (getFragmentManager() != null) {
                        ftrans = getFragmentManager().beginTransaction();
                    }
                    productFragment.setArguments(bundle);
                    Objects.requireNonNull(ftrans).replace(R.id.container, productFragment).addToBackStack("back").commit();
                    Toast.makeText(getActivity(), "Дува", Toast.LENGTH_SHORT).show();
                    break;
                case ("3"):
                    Toast.makeText(getActivity(), "Тири", Toast.LENGTH_SHORT).show();
                    break;
                case ("4"):
                    Toast.makeText(getActivity(), "Читири", Toast.LENGTH_SHORT).show();
                    break;
                case ("5"):
                    ((MainActivity) Objects.requireNonNull(getActivity())).logout();
                    break;
                default:
                    Toast.makeText(getActivity(), "Ошибка!", Toast.LENGTH_SHORT).show();
            }
        }
    };

}
