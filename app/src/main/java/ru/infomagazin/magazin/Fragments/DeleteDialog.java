package ru.infomagazin.magazin.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import java.util.Objects;

import ru.infomagazin.magazin.MainActivity;

public class DeleteDialog extends DialogFragment {

    public String token;

    public String product;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final String[] catNamesArray = {"Удалить"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(catNamesArray, (dialog, which) -> ((MainActivity) Objects.requireNonNull(getActivity())).ShowDialog(getTag()));
        return builder.create();
    }
}
