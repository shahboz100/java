package ru.infomagazin.magazin.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

import ru.infomagazin.magazin.Adapters.OrderAdapter;
import ru.infomagazin.magazin.MainActivity;
import ru.infomagazin.magazin.R;

public class OrderFragmentFull extends Fragment  implements View.OnClickListener{


    RecyclerView orderRecyclerView;
    OrderAdapter orderAdapter;
    String id;
    String name;
    String count;
    String dateP;
    String dateZ;
    String client;
    String note;

    public OrderFragmentFull() {
    }


    public static OrderFragmentFull newInstance() {
        return new OrderFragmentFull();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        assert getArguments() != null;
        id = getArguments().getString("tag", "0");
        name = getArguments().getString("name", "none");
        count = getArguments().getString("count", "0");
        dateP = getArguments().getString("dateP", "1.01.2019");
        dateZ = getArguments().getString("dateZ", "1.01.2019");
        client = getArguments().getString("client", "None");
        note = getArguments().getString("note", "None");
        View rootView =
                inflater.inflate(R.layout.fragment_order_full, container, false);
        Button button1 = rootView.findViewById(R.id.buttonCommentFull);
        button1.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView product = view.findViewById(R.id.nameF);
        TextView col = view.findViewById(R.id.countFullOrder);
        TextView datep = view.findViewById(R.id.datePfull);
        TextView datez = view.findViewById(R.id.dateZfull);
        TextView cl = view.findViewById(R.id.clientFullOrder);
        TextView not = view.findViewById(R.id.noteFullOrder);

        product.setText(name);
        col.setText(count);
        datep.setText(dateP);
        datez.setText(dateZ);
        cl.setText(client);
        not.setText(note);
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainActivity) Objects.requireNonNull(getActivity())).onVisibleNavigation(true);
    }

    @Override
    public void onClick(View v) {
        ((MainActivity) Objects.requireNonNull(getActivity())).onNavigationItemSelected(String.valueOf(id));
    }
}
