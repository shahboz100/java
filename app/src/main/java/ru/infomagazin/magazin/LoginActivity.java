package ru.infomagazin.magazin;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import ru.infomagazin.magazin.HttpRequests.Authentication;
import ru.infomagazin.magazin.HttpRequests.TokenManager;

public class LoginActivity extends Activity {

    private String token;
    private String login;
    private String ROLE;
    private String password;
    private boolean flag = false;
    Auth mt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void onClick(View view) {

        mt = new Auth();
        mt.execute();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mt != null) {
            mt.cancel(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        TextView errorText = findViewById(R.id.auth_error);
        ProgressBar bar = findViewById(R.id.login_progress);
        Button btn_login = findViewById(R.id.email_sign_in_button);
        bar.setVisibility(View.GONE);
        errorText.setVisibility(View.GONE);
        btn_login.setVisibility(View.VISIBLE);
    }

    public void onClickRegister(View view) {
        Intent intent11 = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent11);
    }

    @SuppressLint("StaticFieldLeak")
    class Auth extends AsyncTask<String, Void, String> {

        TextView errorText = findViewById(R.id.auth_error);
        ProgressBar bar = findViewById(R.id.login_progress);
        Button btn_login = findViewById(R.id.email_sign_in_button);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //Отключение видимости кнопки входа
            btn_login.setVisibility(View.GONE);

            // Включение видимости процесс бара (Proccess bar)
            bar.setVisibility(View.VISIBLE);

            //Выключаем видимость строки ошибки
            errorText.setVisibility(View.GONE);

        }

        @Override
        protected String doInBackground(String... url) {

            Authentication auth = new Authentication();
            EditText phone = findViewById(R.id.email);
            EditText ed_password = findViewById(R.id.password);
            String str_token;
            try {
                //Получение токена (Авторизация)
                login = phone.getText().toString();
                password = ed_password.getText().toString();
                str_token = auth.http_auth(login, password);
                JSONObject json_token = new JSONObject(str_token);
                //
                // Если JSON не содержит "error" то получим токен
                // C помошью полученного токена получим информацию о водителе
                //
                if (!json_token.has("error")) {
                    //
                    // Установим флагу true, чтобы получить доступ к аккаунту
                    //
                    flag = true;
                    // Получение токена из JSON
                    token = json_token.getString("token");
                    ROLE = json_token.getString("role");

                } else {
                    token = json_token.getString("error");
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return token; //Метод возвращает токен!
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (flag) {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                TokenManager tokenManager = new TokenManager();
                tokenManager.setToken(result);
                intent.putExtra("login", login);
                intent.putExtra("password", password);
                intent.putExtra("token",result);
                intent.putExtra("role",ROLE);
                startActivity(intent);
            } else {
                errorText.setText(result);
                errorText.setVisibility(View.VISIBLE);
                bar.setVisibility(View.GONE);
                btn_login.setVisibility(View.VISIBLE);
            }
        }
    }
}
