package ru.infomagazin.magazin;

import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ru.infomagazin.magazin.Fragments.CommentFragment;
import ru.infomagazin.magazin.Fragments.DeleteDialog;
import ru.infomagazin.magazin.Fragments.MenuFragment;
import ru.infomagazin.magazin.Fragments.OrderFragment;
import ru.infomagazin.magazin.HttpRequests.HttpRequest;

public class MainActivity extends AppCompatActivity  {
    ///
    public static final String APP_PREFERENCES = "user_settings";
    public static final String APP_PREFERENCES_LOGIN = "login";
    public static final String APP_PREFERENCES_PASSWORD = "password";
    public static String ROLE;
    private SharedPreferences mSettings;
    String token;
    boolean flag=false;

    public String product_id;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
                FragmentManager fm = getSupportFragmentManager();
                Intent intent = getIntent();
                Bundle bundle = new Bundle();

                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        bundle.clear();
                        Fragment fragment = new OrderFragment();

                        bundle.putString("token", token);
                        bundle.putString("role", ROLE);
                        fragment.setArguments(bundle);

                        fm.beginTransaction().add(R.id.container, fragment).commit();
                        return true;
                    case R.id.navigation_menu:
                        bundle.clear();
                        String token = intent.getStringExtra("token");
                        bundle.putString("token", token);
                        Fragment menuFragment = new MenuFragment();
                        menuFragment.setArguments(bundle);
                        fm.beginTransaction().add(R.id.container, menuFragment).commit();

                        return true;
                    case R.id.navigation_messages:
                        bundle.clear();
                        return true;
                }
                return false;
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Bundle bundle = new Bundle();

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = new OrderFragment();

        Intent intent = getIntent();
        token = intent.getStringExtra("token");
        ROLE = intent.getStringExtra("role");
        bundle.putString("token", token);
        fragment.setArguments(bundle);
        fm.beginTransaction().add(R.id.container, fragment).commit();

        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(MainActivity.this, String.valueOf(task.getException()),Toast.LENGTH_SHORT).show();
                            return;
                        }
                        // Get new Instance ID token
                        String token1 = task.getResult().getToken();
                        SendKeyFirebase sendKeyFirebase = new SendKeyFirebase();
                        sendKeyFirebase.execute(token1);
                    }
                });
    }

    public void onNavigationItemSelected(String id) {
        product_id = id;
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = new CommentFragment();

        Intent intent = getIntent();
        String token = intent.getStringExtra("token");

        Bundle bundle = new Bundle();
        bundle.putString("token", token);
        bundle.putString("id", id);
        onVisibleNavigation(false);
        fragment.setArguments(bundle);

        fm.beginTransaction().add(R.id.container, fragment).addToBackStack("back").commit();
    }

    public void onSelectedDialogDelete(String id){

        DeleteDialog deleteDialog = new DeleteDialog();
        FragmentManager manager = getSupportFragmentManager();
        deleteDialog.token = token;
        FragmentTransaction transaction = manager.beginTransaction();
        deleteDialog.show(transaction, id);
    }

    public void onVisibleNavigation(boolean visible){
        //Отключаем видимость нижнего меню
        BottomNavigationView navigation  = findViewById(R.id.navigation);
        if (visible){
            navigation.setVisibility(View.VISIBLE);
        }
        else {
            navigation.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!flag){
            // Запоминаем данные при закрытии
            Intent intent = getIntent();
            SharedPreferences.Editor editor = mSettings.edit();
            editor.putString(APP_PREFERENCES_LOGIN, intent.getStringExtra("login"));
            editor.putString(APP_PREFERENCES_PASSWORD, intent.getStringExtra("password"));
            editor.apply();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        onVisibleNavigation(true);
    }

    public void ShowDialog(String id){
        DeleteTask deleteDialog = new DeleteTask(this);
        deleteDialog.execute(id);
    }

    class DeleteTask extends AsyncTask<String,Void, JSONObject> {

        private MainActivity mainActivity;

        DeleteTask(MainActivity context){
            this.mainActivity = context;
        }

        @Override
        protected JSONObject doInBackground(String[] url) {

            HttpRequest httpRequest = new HttpRequest();
            try {
                return httpRequest.deleteComment(token, url[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            if (result.has("message")){
                Toast toast = null;
                try {
                    toast = Toast.makeText(mainActivity.getApplicationContext(),
                            result.getString("message"), Toast.LENGTH_SHORT);
                    toast.show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            FragmentManager fm = getSupportFragmentManager();
            Fragment fragment = new CommentFragment();

            Intent intent = getIntent();
            String token = intent.getStringExtra("token");

            Bundle bundle = new Bundle();
            bundle.putString("token", token);
            bundle.putString("id", mainActivity.product_id);
            onVisibleNavigation(false);
            fragment.setArguments(bundle);

            fm.beginTransaction().add(R.id.container, fragment).commit();
        }
    }

    public void logout(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Вы действительно хотите выйти?")
                .setPositiveButton("Выйти",
                        (dialog, id) -> {
                            SharedPreferences.Editor editor = mSettings.edit();
                            flag=true;
                            editor.remove(APP_PREFERENCES_LOGIN);
                            editor.remove(APP_PREFERENCES_PASSWORD);
                            editor.apply();
                            finish();
                        })
                .setNegativeButton("Отмена",
                        (dialog, which) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
    }

    class SendKeyFirebase extends AsyncTask<String,Void, Void>{

        @Override
        protected Void doInBackground(String[] url) {

            HttpRequest httpRequest = new HttpRequest();
            try {
                httpRequest.setKeyFirebase(token, url[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }
}
