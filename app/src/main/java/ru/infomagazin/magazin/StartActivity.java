package ru.infomagazin.magazin;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ru.infomagazin.magazin.HttpRequests.Authentication;

public class StartActivity extends Activity {

    public static final String APP_PREFERENCES = "user_settings";
    public static final String APP_PREFERENCES_LOGIN = "login";
    public static final String APP_PREFERENCES_PASSWORD = "password";
    private SharedPreferences mSettings;

    public String login;
    public String password;
    public String token;
    public boolean flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_activity);
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mSettings.contains(APP_PREFERENCES_LOGIN)) {
            Start start = new Start();
            start.execute();
        } else {
            Intent intent;
            intent = new Intent(StartActivity.this, LoginActivity.class);
            startActivity(intent);
        }

    }

    @SuppressLint("StaticFieldLeak")
    class Start extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {

            Intent intent;

            Authentication auth = new Authentication();

            String str_token;
            try {
                //Получение токена (Авторизация)
                login = mSettings.getString(APP_PREFERENCES_LOGIN, "");
                password = mSettings.getString(APP_PREFERENCES_PASSWORD, "");
                str_token = auth.http_auth(login, password);
                JSONObject json_token = new JSONObject(str_token);
                //
                // Если JSON не содержит "error" то получим токен
                // C помошью полученного токена получим информацию о водителе
                //
                if (!json_token.has("error")) {
                    //
                    // Установим флагу true, чтобы получить доступ к аккаунту
                    //
                    flag = true;
                    // Получение токена из JSON
                    token = json_token.getString("token");

                } else {
                    token = json_token.getString("error");
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            // Получаем Имя и Фамилию из настрое
            intent = new Intent(StartActivity.this, MainActivity.class);
            intent.putExtra("login", login);
            intent.putExtra("password", password);
            intent.putExtra("token", token);
            startActivity(intent);
            return null;
        }
    }
}
