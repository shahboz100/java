package ru.infomagazin.magazin.Adapters;

import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.infomagazin.magazin.Model.CommentModel;
import ru.infomagazin.magazin.R;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.commentHolder> {

    List<CommentModel> commentModels = new ArrayList<>();
    View.OnClickListener listener;

    public CommentAdapter(View.OnClickListener listener) {
        super();
        this.listener = listener;
    }

    public void setCommentList(List<CommentModel> commentModels) {
        this.commentModels = commentModels;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CommentAdapter.commentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_list_item, parent, false);
        return new commentHolder(v);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(@NonNull CommentAdapter.commentHolder holder, int position) {
        holder.bindView(commentModels.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return commentModels.size();
    }

    public static class commentHolder extends RecyclerView.ViewHolder {
        TextView author = itemView.findViewById(R.id.author);
        TextView date = itemView.findViewById(R.id.commentDate);
        TextView comment = itemView.findViewById(R.id.comment);

        public commentHolder(View itemView) {
            super(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        private void bindView(CommentModel model, View.OnClickListener listener) {

            itemView.setTag(model.id);
            itemView.setOnClickListener(listener);
            author.setText(model.author);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

            if (model.date.equals(dateFormat.format(new Date()))){
                date.setText(model.time);
            }
            else{
                date.setText(model.date);
            }
            comment.setText(model.comment);

        }
    }
}
