package ru.infomagazin.magazin.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.infomagazin.magazin.Model.MenuModel;
import ru.infomagazin.magazin.Model.OrderModel;
import ru.infomagazin.magazin.R;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.menuHolder> {


    List<MenuModel> menuModels = new ArrayList<>();
    View.OnClickListener listener;

    public MenuAdapter(View.OnClickListener listener) {
        super();
        this.listener = listener;
    }

    public void setMenuList(List<MenuModel> menuModels) {
        this.menuModels = menuModels;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public menuHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_list_item, parent, false);
        menuHolder vh = new menuHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MenuAdapter.menuHolder holder, int position) {
        holder.bindView(menuModels.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return menuModels.size();
    }

    public static class menuHolder extends RecyclerView.ViewHolder {
        TextView name = (TextView) itemView.findViewById(R.id.name);

        public menuHolder(@NonNull View itemView) {
            super(itemView);
        }

        private void bindView(MenuModel model, View.OnClickListener listener) {
            itemView.setTag(model.id);
            itemView.setOnClickListener(listener);
            name.setText(model.name);
        }
    }
}
