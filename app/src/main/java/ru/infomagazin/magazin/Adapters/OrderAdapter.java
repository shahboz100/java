package ru.infomagazin.magazin.Adapters;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.infomagazin.magazin.Model.OrderModel;
import ru.infomagazin.magazin.R;


public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.orderHolder> {

    List<OrderModel> orderModels = new ArrayList<>();
    View.OnClickListener listener;

    public OrderAdapter(View.OnClickListener listener) {
        super();
        this.listener = listener;
    }

    public void setDriverList(List<OrderModel> driverModels) {
        this.orderModels = driverModels;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public orderHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_list_item, parent, false);
        orderHolder vh = new orderHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull orderHolder holder, int position) {
        holder.bindView(orderModels.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return orderModels.size();
    }

    public static class orderHolder extends RecyclerView.ViewHolder {
        ImageView confirmIcon = itemView.findViewById(R.id.confirmIcon);
        ImageView commentIcon = itemView.findViewById(R.id.commentIcon);
        TextView product = itemView.findViewById(R.id.product);
        TextView date = itemView.findViewById(R.id.dateSozdaniya);
        TextView client = itemView.findViewById(R.id.client);
        TextView col = itemView.findViewById(R.id.count);
        TextView dateP = itemView.findViewById(R.id.datePString);

        public orderHolder(View itemView) {
            super(itemView);
        }

        private void bindView(OrderModel model, View.OnClickListener listener) {

            itemView.setTag(model.id);
            itemView.setOnClickListener(listener);
            confirmIcon.setTag(model.id);
            confirmIcon.setOnClickListener(listener);
            commentIcon.setTag(model.id);
            commentIcon.setOnClickListener(listener);
            product.setText(model.product);
            date.setText(model.createDate);
            client.setText(model.client);
            col.setText(String.valueOf(model.count));
            dateP.setText(model.date);
            if (model.confirm)
                confirmIcon.setColorFilter(Color.parseColor("#40bf21"));
        }
    }
}