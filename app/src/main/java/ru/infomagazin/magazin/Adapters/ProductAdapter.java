package ru.infomagazin.magazin.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.infomagazin.magazin.Model.ProductModel;
import ru.infomagazin.magazin.R;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.productHolder> {

    List<ProductModel> productModels = new ArrayList<>();
    View.OnClickListener listener;

    public ProductAdapter(View.OnClickListener listener) {
        super();
        this.listener = listener;
    }

    public void setProductList(List<ProductModel> productModels) {
        this.productModels = productModels;
        notifyDataSetChanged();
    }


    @Override
    public ProductAdapter.productHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item_list, parent, false);
        ProductAdapter.productHolder vh = new ProductAdapter.productHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductAdapter.productHolder holder, int position) {
        holder.bindView(productModels.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return productModels.size();
    }

    public static class productHolder extends RecyclerView.ViewHolder {
        TextView product_name = itemView.findViewById(R.id.product_name);
        TextView product_price = itemView.findViewById(R.id.product_price);
        TextView product_category = itemView.findViewById(R.id.product_category);
        TextView product_count = itemView.findViewById(R.id.product_count);

        public productHolder(View itemView) {
            super(itemView);
        }

        private void bindView(ProductModel model, View.OnClickListener listener) {

            itemView.setTag(model.id);
            itemView.setOnClickListener(listener);
            product_name.setText(model.name);
            product_price.setText(model.price);
            product_category.setText(model.category);
            product_count.setText(model.count);
        }
    }
}